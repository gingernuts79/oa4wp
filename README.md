# README #
This is a new instance of the Open Australia Plugin for WP.
With the Aim to combine this and poli-press the plugin into a single new plugin, that is up to date and offers new connections to the other opendata sources available.

### What is this repository for? ###

* This is a development fork or new instance of the old plugin to try and combine the efforts of this plugin and polipress into a updated plugin for wordpress. Combineing the best of both into a single package.


## 
Original PLUGIN OpenAustralia.org for Wordpress ##


This is a Wordpress plugin that displays your MP's most recent speeches from OpenAustralia.org on your blog. 
It is adapted from the TheyWorkForYou plugin by Philip John: http://philipjohn.co.uk/category/plugins/theyworkforyou/

The easiest way to install it on your blog is to download this source as a zip file (use the Github Download source button above) and go to Plugins>Add New>Upload on your Wordpress blog. For other ways of installing plugins see http://urbangiraffe.com/articles/how-to-install-a-wordpress-plugin/

To report bugs or request features, email the OpenAustralia Community list: http://groups.google.com/group/openaustralia-dev/
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact